package controlador.salario;

import java.util.List;

import modelo.fachada.FachadaRepositorio;
import modelo.salario.Salario;

public class SalarioControlador {

	private FachadaRepositorio repositorio;

	public SalarioControlador() {
		repositorio = new FachadaRepositorio();
	}

	public void adcionarSalario(Salario salario) {
		this.repositorio.adcionarSalario(salario);

	}

	public void removerSalario(int id) {
		this.repositorio.removerSalario(id);
	}

	public void atualizarSalario(Salario salario) {
		this.repositorio.atualizarSalario(salario);

	}
	public List<Salario> listarSalario(int id) {
		return repositorio.listarSalario(id);
	}
	public void atualizarSalarioRestante(double salarioRestante,double saida , int idSalario) {
		this.repositorio.atualizarSalarioRestante(salarioRestante, saida, idSalario);
	}
	public List<Salario>listarSalarioRelatorio(int id){
		return repositorio.listarSalarioRelatorio(id);
	}

}
