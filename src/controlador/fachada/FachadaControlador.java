package controlador.fachada;

import java.util.List;

import controlador.cliente.ClienteControlador;

import controlador.despesa.DespesaControlador;

import controlador.salario.SalarioControlador;
import modelo.cliente.Cliente;
import modelo.cliente.ClienteIFachada;
import modelo.despesa.Despesa;
import modelo.despesa.DespesaIFachada;
import modelo.salario.Salario;
import modelo.salario.SalarioIFachada;

public class FachadaControlador implements ClienteIFachada, DespesaIFachada, SalarioIFachada {

	private static ClienteControlador clienteInstancia;
	private static DespesaControlador despesaInstancia;
	private static SalarioControlador salarioInstancia;

	 FachadaControlador() {
		clienteInstancia = new ClienteControlador();
		despesaInstancia = new DespesaControlador();
		salarioInstancia = new SalarioControlador();

	}

	public static SalarioControlador getInstanciaSalario() {
		if (salarioInstancia == null) {
			salarioInstancia = new SalarioControlador();
		}
		return salarioInstancia;
	}

	public static DespesaControlador getInstanciaDespesa() {
		if (despesaInstancia == null) {
			despesaInstancia = new DespesaControlador();
		}

		return despesaInstancia;
	}

	public static ClienteControlador getInstanciaCliente() {
		if (clienteInstancia == null) {
			clienteInstancia = new ClienteControlador();
		}

		return clienteInstancia;
	}

	@Override
	public void adcionarSalario(Salario salario) {
		salarioInstancia.adcionarSalario(salario);

	}

	@Override
	public void cadastrarDespesa(Despesa d) {
		despesaInstancia.cadastrarDespesa(d);

	}

	@Override
	public void removerSalario(int id) {
		salarioInstancia.removerSalario(id);
	}

	@Override
	public void atualizarSalario(Salario salario) {
		salarioInstancia.atualizarSalario(salario);

	}

	@Override
	public List<Despesa> listarDespesas(int id) {
		return despesaInstancia.listarDespesas(id);
	}

	@Override
	public List<Salario> listarSalario(int id) {
		return salarioInstancia.listarSalario(id);
	}

	@Override
	public void cadastrarCliente(Cliente c) throws Exception {
		clienteInstancia.cadastrarCliente(c);

	}

	@Override
	public Cliente loginAcesso(String email, String senha) {
		// TODO Auto-generated method stub
		return clienteInstancia.loginAcesso(email, senha);
	}

	@Override
	public void alterarSenha(Cliente cliente) throws Exception {
		clienteInstancia.alterarSenha(cliente);

	}

	public void removerDespesa(Despesa despesa) {
		despesaInstancia.removerDespesa(despesa);
	}

	@Override
	public Cliente verificarExistencia(String email) {
		return clienteInstancia.verificarExistencia(email);
	}

	@Override
	public List<Despesa> listarDespesasSelecionada(int id) {
		return despesaInstancia.listarDespesasSelecionada(id);
	}

	@Override
	public void atualizarDespesa(Despesa despesa) {
		despesaInstancia.atualizarDespesa(despesa);
	}
	public void atualizarSalarioRestante(double salarioRestante,double saida , int idSalario) {
		salarioInstancia.atualizarSalarioRestante(salarioRestante, saida, idSalario);
	}
	public List<Salario>listarSalarioRelatorio(int id){
		return listarSalarioRelatorio(id);
	}
	public void excluirCliente (int id) {
		clienteInstancia.excluirCliente(id);
	}

}
