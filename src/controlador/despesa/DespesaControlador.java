package controlador.despesa;

import java.util.List;

import modelo.despesa.Despesa;
import modelo.despesa.DespesaIFachada;
import modelo.fachada.FachadaRepositorio;

public class DespesaControlador implements DespesaIFachada {

	private static FachadaRepositorio repositorio;

	public DespesaControlador() {
		repositorio = new FachadaRepositorio();

	}

	public void cadastrarDespesa(Despesa d) {
		repositorio.cadastrarDespesa(d);
	}
	public List<Despesa> listarDespesas(int id) {
		return repositorio.listarDespesas(id);
	}
	public void removerDespesa(Despesa despesa) {
		repositorio.removerDespesa(despesa);
	}
	public List<Despesa> listarDespesasSelecionada(int id) {
		return repositorio.listarDespesasSelecionada(id);
	}
	public void atualizarDespesa(Despesa despesa) {
		repositorio.atualizarDespesa(despesa);
	}
}
