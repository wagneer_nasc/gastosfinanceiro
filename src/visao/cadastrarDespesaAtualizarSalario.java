package visao;

import java.security.spec.DSAGenParameterSpec;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.EntityManager;

import controlador.fachada.FachadaControlador;
import entityManagerJPA.EntityManagerUtil;
import modelo.despesa.Despesa;
import modelo.despesa.DespesaRepositorio;
import modelo.salario.Salario;
import modelo.salario.SalarioRepositorio;

public class cadastrarDespesaAtualizarSalario {

	public static void main(String[] args) {

		EntityManager em = EntityManagerUtil.getEntityManager();
		
		// adcionando uma despesa a um cliente e atualizando o salario

		
		  Salario s = new Salario();
		  Despesa d = new Despesa();
		  
		
		  double despesa = 500; 
		 
		// Excluindo uma despesa e atualizando o Salario

		
		  d = em.find(Despesa.class, 13);
		  
		  s = em.find(Salario.class, 1);
		  
		  double salarioAtual = s.getValor() + d.getValor();
		  
		  s.setValor(salarioAtual);
		  
		//  FachadaControlador.getInstanciaDespesa().removerDespesa(d);
		  
		  FachadaControlador.getInstanciaSalario().atualizarSalario(s);
		 
		
		  
		  
		  Date da = new Date (); 
		  d.setDescricao("compras no shoop"); 
		  d.setDataDespesa(da); 
		  d.setValor(40);
		 
		  d.setSalario(em.find(Salario.class, 1));
		  
		  FachadaControlador.getInstanciaDespesa().cadastrarDespesa(d);
		 
		 
		
		
		
	}

}
