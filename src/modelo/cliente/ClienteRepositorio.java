package modelo.cliente;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import entityManagerJPA.EntityManagerUtil;

public class ClienteRepositorio implements ClienteIFachada {

	EntityManager em = EntityManagerUtil.getEntityManager();

	public void cadastrarCliente(Cliente c) {
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();

	}

	@Override
	public Cliente loginAcesso(String email, String senha) {
		try {
			Cliente cliente = (Cliente) em
					.createQuery("SELECT cliente from Cliente cliente where cliente.email= \r\n"
							+ "	             :email and cliente.senha = :senha")
					.setParameter("email", email).setParameter("senha", senha).getSingleResult();

			return cliente;
		} catch (NoResultException e) {
			return null;
		}

	}

	public void alterarSenha(Cliente cliente) {
		// TODO Auto-generated method stub
		em.getTransaction().begin();
		em.merge(cliente);
		em.getTransaction().commit();

	}

	public Cliente verificarExistencia(String email) {
		try {
			Cliente cliente = (Cliente) em
					.createQuery("SELECT cliente from Cliente cliente where cliente.email= :email")
					.setParameter("email", email).getSingleResult();

			return cliente;
		} catch (NoResultException e) {

			return null;
		}

	}
	public void excluirCliente (int id) {
		Cliente cliente = em.find(Cliente.class, id);
		em.getTransaction().begin();
		em.remove(cliente);
		em.getTransaction().commit();
	}

}
