package modelo.beans;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.PrimeFaces;

import controlador.fachada.FachadaControlador;
import controlador.session.SessionContext;
import modelo.cliente.Cliente;
import modelo.despesa.Despesa;
import modelo.salario.Salario;

@ManagedBean
public class RelatoriosBeans {

	Cliente cliente = (Cliente) SessionContext.getInstance().getAttribute("usuariologado");
	private List<Salario> listaSalario;
	private Salario salario;
	private List<Despesa> ListaDespesas;
	private double salarioEntrada;
	private double valorTotalDespesas;
	private double saldo;
	private Date dataCadastro;

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public double getSalarioEntrada() {
		return salarioEntrada;
	}

	public void setSalarioEntrada(double salarioEntrada) {
		this.salarioEntrada = salarioEntrada;
	}

	public double getValorTotalDespesas() {
		return valorTotalDespesas;
	}

	public void setValorTotalDespesas(double valorTotalDespesas) {
		this.valorTotalDespesas = valorTotalDespesas;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public List<Despesa> getListaDespesas() {
		return ListaDespesas;
	}

	public void setListaDespesas(List<Despesa> listaDespesas) {
		ListaDespesas = listaDespesas;
	}

	public List<Salario> getListaSalario() {

		this.listaSalario = FachadaControlador.getInstanciaSalario().listarSalarioRelatorio(cliente.getId());
		for (Salario salario : listaSalario) {
			System.out.println(salario.getSaida());
			System.out.println(salario.getCliente().getNome());
		}
		return listaSalario;
	}

	public void setListaSalario(List<Salario> listaSalario) {
		this.listaSalario = listaSalario;
	}

	public Salario getSalario() {
		if (this.salario == null) {
			this.salario = new Salario();
		}

		return salario;
	}

	public void setSalario(Salario salario) {
		this.salario = salario;
	}

	public String abrirDetalhesDespesas() {

		String id = SessionContext.getInstance().getParametroId("idSalario");
		int idSalario = Integer.parseInt(id);
		this.ListaDespesas = FachadaControlador.getInstanciaDespesa().listarDespesas(idSalario);

		for (Salario s : this.listaSalario) {
			if (s.getId() == idSalario) {

				dataCadastro = s.getData();
				salarioEntrada = s.getValor();
				valorTotalDespesas = s.getSaida();
				saldo = s.getSalarioRestante();
			}
		}
		 
		PrimeFaces.current().executeScript("$('.detalheDespesa').modal()");
		return null;
	}

}
