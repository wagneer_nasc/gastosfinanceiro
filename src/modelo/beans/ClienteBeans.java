package modelo.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import controlador.fachada.FachadaControlador;
import controlador.session.SessionContext;
import modelo.cliente.Cliente;

@ManagedBean
@SessionScoped
public class ClienteBeans {
	private Cliente cliente;
	private String senhaAtual;
	private String confirmarSenha;

	public String getConfirmarSenha() {
		return confirmarSenha;
	}

	public void setConfirmarSenha(String confirmarSenha) {
		this.confirmarSenha = confirmarSenha;
	}

	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}

	private String novaSenha;

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public Cliente getCliente() {

		if (this.cliente == null) {
			this.cliente = new Cliente();
		}
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String logout() {

		SessionContext.getInstance().encerrarSessao();
		return "/login.xhtml?faces-redirect=true";
	}

	public String logar() {
		
		cliente = FachadaControlador.getInstanciaCliente().loginAcesso(cliente.getEmail(), cliente.getSenha());
		if (cliente != null) {
			SessionContext.getInstance().setAttribute("usuariologado", cliente);
			return "/principal/principal.xhtml?faces-redirect=true";
		}

		FacesContext.getCurrentInstance().addMessage("msgValidador", new FacesMessage("Email ou senha incorretos"));
		return null;
	}

	public String salvarCliente() {
		cliente.getNome();
		cliente.getEmail();
		cliente.getSenha();

		Cliente verificarExistenciaEmail = FachadaControlador.getInstanciaCliente()
				.verificarExistencia(cliente.getEmail());

		if (verificarExistenciaEmail == null) {

			if (cliente != null) {
				try {
					FachadaControlador.getInstanciaCliente().cadastrarCliente(cliente);
					return "/login.xhtml?faces-redirect=true";

				} catch (Exception e) {

					FacesContext.getCurrentInstance().addMessage("msgValidador",
							new FacesMessage("ERRO AO CADASTRAR CLIENTE" + e));
				}
			}

		}

		FacesContext.getCurrentInstance().addMessage("msgValidador",
				new FacesMessage("Este email j� possui um cadastro"));

		return null;
	}

	public String alterarSenha() {

		Cliente usuarioLogado = (Cliente) SessionContext.getInstance().getAttribute("usuariologado");

		if (senhaAtual.equals(usuarioLogado.getSenha())) {
			if (novaSenha.equals(confirmarSenha)) {
				cliente.setSenha(novaSenha);

				try {
					FachadaControlador.getInstanciaCliente().alterarSenha(cliente);
					return "principal.xhtml?faces-redirect=true";
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				FacesContext.getCurrentInstance().addMessage("novaSenha",
						new FacesMessage("As senha n�o est�o iguais. "));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage("msgSenhaAtual",
					new FacesMessage("Senha atual est� incorreta, digite sua senha que voc� fez o login"));
		}

		return null;
	}

}
