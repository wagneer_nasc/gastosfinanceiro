package modelo.beans;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

import org.primefaces.PrimeFaces;

import controlador.fachada.FachadaControlador;
import controlador.session.SessionContext;
import entityManagerJPA.EntityManagerUtil;
import modelo.cliente.Cliente;
import modelo.despesa.Despesa;
import modelo.salario.Salario;

@ManagedBean
@SessionScoped
public class PrincipalBeans {

	EntityManager em = EntityManagerUtil.getEntityManager();

	Cliente cliente = (Cliente) SessionContext.getInstance().getAttribute("usuariologado");
	private double salarioAtual;
	private int idSalarioAtual;
	private Date dataSalarioAtual;
	private Salario salario;
	private List<Salario> listaSalario;
	private double salarioRestante;
	private List<Despesa> listaDespesas;
	private Despesa despesa;
	private List<Despesa> despesaSelecionada;
	private String descricaoDespesaSelecionada;
	private Date dataDespesaSelecionada;
	private double valorDespesaSelecionada;
	private int idDespesaSelecionada;
	private double novoSalario;
	private Date dateNovoSalario;

	public double getNovoSalario() {
		return novoSalario;
	}

	public void setNovoSalario(double novoSalario) {
		this.novoSalario = novoSalario;
	}

	public Date getDateNovoSalario() {
		return dateNovoSalario;
	}

	public void setDateNovoSalario(Date dateNovoSalario) {
		this.dateNovoSalario = dateNovoSalario;
	}

	public Despesa getDespesa() {
		if (this.despesa == null) {
			this.despesa = new Despesa();
		}
		return despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public List<Despesa> getDespesaSelecionada() {
		return despesaSelecionada;
	}

	public void setDespesaSelecionada(List<Despesa> despesaSelecionada) {
		this.despesaSelecionada = despesaSelecionada;
	}

	public String getDescricaoDespesaSelecionada() {
		return descricaoDespesaSelecionada;
	}

	public void setDescricaoDespesaSelecionada(String descricaoDespesaSelecionada) {
		this.descricaoDespesaSelecionada = descricaoDespesaSelecionada;
	}

	public Date getDataDespesaSelecionada() {
		return dataDespesaSelecionada;
	}

	public void setDataDespesaSelecionada(Date dataDespesaSelecionada) {
		this.dataDespesaSelecionada = dataDespesaSelecionada;
	}

	public double getValorDespesaSelecionada() {
		return valorDespesaSelecionada;
	}

	public void setValorDespesaSelecionada(double valorDespesaSelecionada) {
		this.valorDespesaSelecionada = valorDespesaSelecionada;
	}

	public int getIdDespesaSelecionada() {
		return idDespesaSelecionada;
	}

	public void setIdDespesaSelecionada(int idDespesaSelecionada) {
		this.idDespesaSelecionada = idDespesaSelecionada;
	}

	public List<Despesa> getListaDespesas() {
		this.listaDespesas = FachadaControlador.getInstanciaDespesa().listarDespesas(idSalarioAtual);
		return listaDespesas;
	}

	public double getSalarioRestante() {
		return salarioRestante;
	}

	public void setSalarioRestante(double salarioRestante) {
		this.salarioRestante = salarioRestante;
	}

	public List<Salario> getListaSalario() {
		return this.listaSalario;
	}

	public void setListaSalario(List<Salario> listaSalario) {
		this.listaSalario = listaSalario;
	}

	public int getIdSalarioAtual() {
		return idSalarioAtual;
	}

	public void setIdSalarioAtual(int idSalarioAtual) {
		this.idSalarioAtual = idSalarioAtual;
	}

	public Date getDataSalarioAtual() {
		return dataSalarioAtual;
	}

	public void setDataSalarioAtual(Date dataSalarioAtual) {
		this.dataSalarioAtual = dataSalarioAtual;
	}

	public double getSalarioAtual() {
		return salarioAtual;
	}

	public void setSalarioAtual(double salarioAtual) {
		this.salarioAtual = salarioAtual;
	}

	public void setSalario(Salario salario) {
		this.salario = salario;
	}

	@PostConstruct
	public void iniciaSalarioAtual() {
		this.listaSalario = FachadaControlador.getInstanciaSalario().listarSalario(cliente.getId());
		for (Salario salario : listaSalario) {
			salarioAtual = salario.getValor();
			dataSalarioAtual = salario.getData();
			idSalarioAtual = salario.getId();
			salarioRestante = salario.getSalarioRestante();

		}
	}

	public Salario getSalario() {
		if (this.salario == null) {
			this.salario = new Salario();
		}
		return this.salario;
	}

	public String cadastrarSalario() {

		if (this.salario != null) {

			salario.setSalarioRestante(salario.getValor());
			this.salario.setCliente(em.find(Cliente.class, cliente.getIdCliente()));
			FachadaControlador.getInstanciaSalario().adcionarSalario(salario);

			iniciaSalarioAtual();
			this.salario = new Salario();
		}

		return "/principal/principal.xhtml?faces-redirect=true";
	}

	public String cadastrarNovoSalario() {

		salario.setValor(novoSalario);
		salario.setData(dateNovoSalario);
		salario.setSalarioRestante(salario.getValor());
		this.salario.setCliente(em.find(Cliente.class, cliente.getIdCliente()));
		FachadaControlador.getInstanciaSalario().adcionarSalario(salario);

		iniciaSalarioAtual();
		this.salario = new Salario();
		novoSalario = salario.getValor();
		dateNovoSalario = salario.getData();
		return "/principal/principal.xhtml?faces-redirect=true";
	}

	public String editarSalario() {
		double diferenca;
		double ultimoSalario = 0;
		this.listaSalario = FachadaControlador.getInstanciaSalario().listarSalario(cliente.getId());
		for (Salario salario : listaSalario) {
			ultimoSalario = salario.getValor();
		}
		if (salarioAtual > ultimoSalario) {
			diferenca = salarioAtual - ultimoSalario;
			salarioRestante = salarioRestante + diferenca;
		}
		if (salarioAtual < ultimoSalario) {
			diferenca = ultimoSalario - salarioAtual;
			salarioRestante = salarioRestante - diferenca;
		}
		this.salario.setId(idSalarioAtual);
		this.salario.setData(dataSalarioAtual);
		this.salario.setValor(salarioAtual);
		this.salario.setSalarioRestante(salarioRestante);
		this.salario.setCliente(em.find(Cliente.class, cliente.getIdCliente()));
		FachadaControlador.getInstanciaSalario().atualizarSalario(salario);
		double saida = salarioAtual - salarioRestante;

		FachadaControlador.getInstanciaSalario().atualizarSalarioRestante(salarioRestante, saida, idSalarioAtual);

		iniciaSalarioAtual();
		this.salario = new Salario();
		novoSalario = salario.getValor();
		dateNovoSalario = salario.getData();
		return "/principal/principal.xhtml?faces-redirect=true";
	}

	public String cadastrarDespesa() {

		if (despesa != null) {

			despesa.setSalario(em.find(Salario.class, idSalarioAtual));

			FachadaControlador.getInstanciaDespesa().cadastrarDespesa(despesa);
			salarioRestante = salarioRestante - despesa.getValor();
			double saida = salarioAtual - salarioRestante;

			FachadaControlador.getInstanciaSalario().atualizarSalarioRestante(salarioRestante, saida, idSalarioAtual);

			iniciaSalarioAtual();
			despesa = new Despesa();
		}
		return "/principal/principal.xhtml?faces-redirect=true";
	}

	public String deleteDespesa() {

		String id = SessionContext.getInstance().getParametroId("idDespesa");
		int idDespesa = Integer.parseInt(id);

		Despesa despesa = em.find(Despesa.class, idDespesa);
		FachadaControlador.getInstanciaDespesa().removerDespesa(despesa);
		salarioRestante = salarioRestante + despesa.getValor();
		double saida = salarioAtual - salarioRestante;
		FachadaControlador.getInstanciaSalario().atualizarSalarioRestante(salarioRestante, saida, idSalarioAtual);

		iniciaSalarioAtual();
		return "/principal/principal.xhtml?faces-redirect=true";
	}

	public String abrirModalEditarDespesa() {

		String id = SessionContext.getInstance().getParametroId("idDespesa");
		int idDespesa = Integer.parseInt(id);

		this.despesaSelecionada = FachadaControlador.getInstanciaDespesa().listarDespesasSelecionada(idDespesa);
		for (Despesa despesa : despesaSelecionada) {
			idDespesaSelecionada = despesa.getId();
			descricaoDespesaSelecionada = despesa.getDescricao();
			valorDespesaSelecionada = despesa.getValor();
			dataDespesaSelecionada = despesa.getDataDespesa();
		}
		PrimeFaces.current().executeScript("$('.editarDespesa').modal()");
		return null;
	}

	public String editarDespesa() {

		double despesaAtual = 0;
		double diferenca;
		this.despesaSelecionada = FachadaControlador.getInstanciaDespesa()
				.listarDespesasSelecionada(idDespesaSelecionada);

		for (Despesa despesa : despesaSelecionada) {
			despesaAtual = despesa.getValor();
		}
		if (valorDespesaSelecionada > despesaAtual) {
			diferenca = valorDespesaSelecionada - despesaAtual;
			salarioRestante = salarioRestante - diferenca;
		}
		if (valorDespesaSelecionada < despesaAtual) {
			diferenca = despesaAtual - valorDespesaSelecionada;
			salarioRestante = salarioRestante + diferenca;
		}

		this.despesa.setIdDespesa(idDespesaSelecionada);
		this.despesa.setValor(valorDespesaSelecionada);
		this.despesa.setDescricao(descricaoDespesaSelecionada);
		this.despesa.setDataDespesa(dataDespesaSelecionada);
		this.despesa.setSalario(em.find(Salario.class, idSalarioAtual));
		FachadaControlador.getInstanciaDespesa().atualizarDespesa(despesa);
		double saida = salarioAtual - salarioRestante;
		FachadaControlador.getInstanciaSalario().atualizarSalarioRestante(salarioRestante, saida, idSalarioAtual);

		iniciaSalarioAtual();
		this.despesa = new Despesa();
		return "/principal/principal.xhtml?faces-redirect=true";
	}

	public String chamarRelatorios() {

		return "/principal/relatorios.xhtml?faces-redirect=true";
	}

}
