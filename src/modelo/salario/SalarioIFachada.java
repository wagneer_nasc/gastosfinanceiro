package modelo.salario;

import java.util.List;

 

public interface SalarioIFachada {
	
	public void adcionarSalario(Salario salario);
	public void removerSalario(int salario);
	public void atualizarSalario(Salario salario);
	public List<Salario> listarSalario(int id) ;
	public void atualizarSalarioRestante(double salarioRestante,double saida , int idSalario);
	public List<Salario>listarSalarioRelatorio(int id);

	 
}
