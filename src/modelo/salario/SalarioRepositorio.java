package modelo.salario;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entityManagerJPA.EntityManagerUtil;

public class SalarioRepositorio implements SalarioIFachada {

	EntityManager em = EntityManagerUtil.getEntityManager();

	public void adcionarSalario(Salario salario) {

		em.getTransaction().begin();
		em.persist(salario);
		em.getTransaction().commit();

	}

	public void removerSalario(int id) {
		Salario salario = em.find(Salario.class, id);
		em.getTransaction().begin();
		em.remove(salario);
		em.getTransaction().commit();
	}

	@Override
	public void atualizarSalario(Salario salario) {
		em.getTransaction().begin();
		em.merge(salario);
		em.getTransaction().commit();

	}

	public List<Salario> listarSalario(int id) {

		String consulta = "Select s From Salario s join s.cliente c where c.idCliente= :id ORDER BY id_salario DESC";
		TypedQuery<Salario> query = em.createQuery(consulta, Salario.class);
		query.setParameter("id", id);
		query.setMaxResults(1);
		List<Salario> resultado = query.getResultList();

		return resultado;
	}

	public void atualizarSalarioRestante(double salarioRestante, double saida, int idSalario) {
		Salario salario = em.find(Salario.class, idSalario);
		em.getTransaction().begin();
		salario.setSalarioRestante(salarioRestante);
		salario.setSaida(saida);
		em.getTransaction().commit();

	}

	public List<Salario> listarSalarioRelatorio(int id) {

		String consulta = ("Select s From Salario s join s.cliente c where c.idCliente= :id ORDER BY id_salario ASC");
		TypedQuery<Salario> query = em.createQuery(consulta, Salario.class);
		query.setParameter("id", id);
		List<Salario> resultado = query.getResultList();
		return resultado;

	}

}
