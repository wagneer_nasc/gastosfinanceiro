package modelo.fachada;

import java.util.List;

import modelo.cliente.Cliente;
import modelo.cliente.ClienteIFachada;

import modelo.cliente.ClienteRepositorio;
import modelo.despesa.Despesa;
import modelo.despesa.DespesaIFachada;

import modelo.despesa.DespesaRepositorio;
import modelo.salario.Salario;
import modelo.salario.SalarioIFachada;
import modelo.salario.SalarioRepositorio;

public class FachadaRepositorio implements ClienteIFachada, DespesaIFachada, SalarioIFachada {

	private static ClienteRepositorio clienteInstancia;
	private static DespesaRepositorio despesaInstancia;
	private static SalarioRepositorio salarioInstancia;

	 public FachadaRepositorio() {
		clienteInstancia = new ClienteRepositorio();
		despesaInstancia = new DespesaRepositorio();
		salarioInstancia = new SalarioRepositorio();
	}

	public static SalarioRepositorio getInstanciaSalario() {
		if (salarioInstancia == null) {
			salarioInstancia = new SalarioRepositorio();
		}
		return salarioInstancia;
	}

	public static DespesaRepositorio getInstanciaDespesa() {
		if (despesaInstancia == null) {
			despesaInstancia = new DespesaRepositorio();
		}
		return despesaInstancia;
	}

	public static ClienteRepositorio getInstancia() {
		if (clienteInstancia == null) {
			clienteInstancia = new ClienteRepositorio();
		}
		return clienteInstancia;
	}

	@Override
	public void cadastrarDespesa(Despesa d) {
		// TODO Auto-generated method stub
		despesaInstancia.cadastrarDespesa(d);

	}

	@Override
	public void adcionarSalario(Salario salario) {
		// TODO Auto-generated method stub
		salarioInstancia.adcionarSalario(salario);

	}

	@Override
	public void removerSalario(int id) {
		salarioInstancia.removerSalario(id);

	}

	@Override
	public void atualizarSalario(Salario salario) {
		salarioInstancia.atualizarSalario(salario);

	}

	@Override
	public List<Salario> listarSalario(int id) {
		return salarioInstancia.listarSalario(id);
	}

	@Override
	public List<Despesa> listarDespesas(int id) {
		return despesaInstancia.listarDespesas(id);
	}

	@Override
	public void cadastrarCliente(Cliente c) throws Exception {
		clienteInstancia.cadastrarCliente(c);

	}

	@Override
	public Cliente loginAcesso(String email, String senha) {
		// TODO Auto-generated method stub
		return clienteInstancia.loginAcesso(email, senha);
	}

	@Override
	public void alterarSenha(Cliente cliente) throws Exception {
		clienteInstancia.alterarSenha(cliente);

	}

	public void removerDespesa(Despesa despesa) {
		despesaInstancia.removerDespesa(despesa);
	}

	@Override
	public Cliente verificarExistencia(String email) {
		return clienteInstancia.verificarExistencia(email);
	}

	@Override
	public List<Despesa> listarDespesasSelecionada(int id) {
		return despesaInstancia.listarDespesasSelecionada(id);
	}

	@Override
	public void atualizarDespesa(Despesa despesa) {
		despesaInstancia.atualizarDespesa(despesa);
	}

	public void atualizarSalarioRestante(double salarioRestante,double saida , int idSalario) {
		salarioInstancia.atualizarSalarioRestante(salarioRestante, saida, idSalario);
	}

	public List<Salario> listarSalarioRelatorio(int id) {
		return salarioInstancia.listarSalarioRelatorio(id);
	}
	public void excluirCliente (int id) {
		clienteInstancia.excluirCliente(id);
	}

}
