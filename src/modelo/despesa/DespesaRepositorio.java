package modelo.despesa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entityManagerJPA.EntityManagerUtil;

public class DespesaRepositorio implements DespesaIFachada {

	EntityManager em = EntityManagerUtil.getEntityManager();

	@Override
	public void cadastrarDespesa(Despesa d) {
		em.getTransaction().begin();
		em.persist(d);
		em.getTransaction().commit();

	}

	public List<Despesa> listarDespesas(int id) {
		String consulta = "Select d From Despesa d join d.salario s where s.id= :id ORDER BY id_despesa DESC";
		TypedQuery<Despesa> query = em.createQuery(consulta, Despesa.class);
		query.setParameter("id", id);
		List<Despesa> resultado = query.getResultList();
		return resultado;
	}

	public void removerDespesa(Despesa despesa) {
		em.getTransaction().begin();
		em.remove(despesa);
		em.getTransaction().commit();
	}

	public void atualizarDespesa(Despesa despesa) {
		em.getTransaction().begin();
		em.merge(despesa);
		em.getTransaction().commit();
	}

	public List<Despesa> listarDespesasSelecionada(int id) {
		String consulta = "Select d From Despesa d where d.id= :id";
		TypedQuery<Despesa> query = em.createQuery(consulta, Despesa.class);
		query.setParameter("id", id);
		List<Despesa> resultado = query.getResultList();
		return resultado;
	}

}
